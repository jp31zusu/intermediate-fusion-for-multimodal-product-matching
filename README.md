# Intermediate Fusion for Multimodal Product Matching
This repository contains the code and documentation for the paper "Intermediate Fusion for Multimodal Product Matching".

## Contents
* **requirements.txt**: Python dependencies

* Python scripts:
    * **data_preparation.py**: Data loading, preparation and pre-processing
    * **models.py**: Building and training of models

* WDC Shoes dataset:
    * **wdc_shoes_data.pickle**: WDC Shoes training, validation, and test data
      * In each case pairs of texts, pairs of image paths, and labels
    * **wdc_show_images**: WDC Shoes images

* Zalando dataset:
    * **zalando_data.pickle**: Zalando training, validation, and test data
      * In each case pairs of texts, pairs of image paths, and labels
    * **zalando_images**: Zalando images

## Setup

* Install requirements 

    ```pip install -r requirements.txt```

* Download Tensorflow Swin-Transformer model: https://tfhub.dev/sayakpaul/swin_base_patch4_window7_224_in22k_fe/1

## Usage
The following code snippet demonstrates how to train and evaluate the proposed Intermediate Fusion model on the *WDC Shoes* dataset as an example.

```python
import data_preparation
import models

data_id = "WDC Shoes"
text_embedding_id = "RoBERTa"
image_embedding_id = "Swin-Transformer"

epochs = 40

model_id = "Fine-tuned"
train_batches, val_batches, test_batches = data_preparation.get_batches(data_id, model_id, text_embedding_id)
fine_tuned_model = models.build_fine_tuned_model(text_embedding_id)
fine_tuned_model = models.train_fine_tuned_model(fine_tuned_model, data_id, train_batches, val_batches, epochs)
fine_tuned_model = models.train_fine_tuned_model(fine_tuned_model, data_id, train_batches, val_batches, epochs, unfreeze=True)
fine_tuned_model.evaluate(test_batches)

model_id = "Siamese"
train_batches, val_batches, test_batches = data_preparation.get_batches(data_id, model_id, text_embedding_id)
siamese_model = models.build_siamese_model(image_embedding_id)
siamese_model = models.train_siamese_model(siamese_model, data_id, train_batches, val_batches, epochs)
siamese_model.evaluate(test_batches)

model_id = "Intermediate Fusion"
train_batches, val_batches, test_batches = data_preparation.get_batches(data_id, model_id, text_embedding_id)
model = models.build_intermediate_fusion_model(fine_tuned_model, siamese_model)
model = models.train_intermediate_fusion_model(model, data_id, train_batches, val_batches, epochs)
model.evaluate(test_batches)
```
