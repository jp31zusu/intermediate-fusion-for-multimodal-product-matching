import data_preparation
import tensorflow as tf
import tensorflow_addons as tfa
import transformers

# Path to Tensorflow Swin-Transformer model
#https://tfhub.dev/sayakpaul/swin_base_patch4_window7_224_in22k_fe/1
path_to_swin_model = "SWIN"

# Metrics for evaluation
metrics = [
    tf.keras.metrics.BinaryAccuracy(name="accuracy"),
    tf.keras.metrics.Precision(name="precision"),
    tf.keras.metrics.Recall(name="recall"),
    tfa.metrics.F1Score(num_classes=1, threshold=0.5, name="f1_score")
]

# Early Stopping
callback = tf.keras.callbacks.EarlyStopping(monitor="f1_score", mode="max", patience=3)


def build_fine_tuned_model(embedding_id):
    """
    Build model with Fine-tuned architecture
    :param embedding_id: embedding network to use
    :return: uncompiled model
    """

    # Transformer inputs
    input_ids = tf.keras.layers.Input(shape=(128,), dtype=tf.int32)
    attention_masks = tf.keras.layers.Input(shape=(128,), dtype=tf.int32)
    token_type_ids = tf.keras.layers.Input(shape=(128,), dtype=tf.int32)

    # Load pre-trained embedding network
    if embedding_id == "RoBERTa":
        embedding_network = transformers.TFRobertaModel.from_pretrained("roberta-base")
        embedding_network.trainable = False

        embedding_network_output = embedding_network.roberta(
            input_ids,
            attention_mask=attention_masks,
            token_type_ids=token_type_ids
        )

    elif embedding_id == "BERT":
        embedding_network = transformers.TFBertModel.from_pretrained("bert-base-cased")
        embedding_network.trainable = False

        embedding_network_output = embedding_network.bert(
            input_ids,
            attention_mask=attention_masks,
            token_type_ids=token_type_ids
        )

    # Build architecture
    sequence_output = embedding_network_output.last_hidden_state
    bi_lstm = tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(64, return_sequences=True))(sequence_output)
    avg_pool = tf.keras.layers.GlobalAveragePooling1D()(bi_lstm)
    max_pool = tf.keras.layers.GlobalMaxPooling1D()(bi_lstm)
    concat = tf.keras.layers.concatenate([avg_pool, max_pool])
    batch_norm = tf.keras.layers.BatchNormalization()(concat)
    dropout_1 = tf.keras.layers.Dropout(0.3)(batch_norm)
    dense_1 = tf.keras.layers.Dense(512, activation="relu", kernel_initializer="he_normal")(dropout_1)
    dropout_2 = tf.keras.layers.Dropout(0.3)(dense_1)
    dense_2 = tf.keras.layers.Dense(256, activation="relu", kernel_initializer="he_normal")(dropout_2)
    dropout_3 = tf.keras.layers.Dropout(0.3)(dense_2)
    output = tf.keras.layers.Dense(1, activation='sigmoid')(dropout_3)

    model = tf.keras.Model(inputs=[input_ids, attention_masks, token_type_ids], outputs=output)

    return model


# Calculate Euclidean distance using Tensorflow
def euclidean_distance(vectors):
    x, y = vectors
    sum_square = tf.math.reduce_sum(tf.math.square(x - y), axis=1, keepdims=True)
    return tf.math.sqrt(tf.math.maximum(sum_square, tf.keras.backend.epsilon()))


def build_siamese_model(embedding_id):
    """
    Build model with Siamese architecture
    :param embedding_id: embedding network to use
    :return: uncompiled model
    """

    # Load pre-trained embedding network
    if embedding_id == "Swin-Transformer":
        embedding_network = tf.keras.models.load_model(path_to_swin_model)

        # Unfreeze the last two layers
        for layer in embedding_network.layers:
            if layer.name != "basic_layer_3":
                layer.trainable = False

        for layer in embedding_network.get_layer("basic_layer_3").layers:
            if layer.name != "swin_transformer_block_1":
                layer.trainable = False

        for layer in embedding_network.get_layer("basic_layer_3").get_layer("swin_transformer_block_1").layers:
            if layer.name != "mlp":
                layer.trainable = False

        for layer in embedding_network.get_layer("basic_layer_3").get_layer("swin_transformer_block_1").get_layer(
                "mlp").layers:
            if layer.name not in ["dense_49", "dense_50"]:
                layer.trainable = False
            else:
                layer.trainable = True

    if embedding_id == "ResNet":
        embedding_network = tf.keras.applications.resnet.ResNet50(
            weights="imagenet",
            input_shape=(224, 224) + (3,),
            include_top=False
        )

        # Unfreeze last block
        trainable = False
        for layer in embedding_network.layers:
            if layer.name == "conv5_block1_out":
                trainable = True
            layer.trainable = trainable

        # Add pooling layer to get one-dimensional vector
        pool = tf.keras.layers.GlobalAveragePooling2D()(embedding_network.output)
        embedding_network = tf.keras.Model(embedding_network.input, pool)

    # Build sub-network architecture
    image = tf.keras.layers.Input((224, 224) + (3,))
    embedding = embedding_network(image, training=False)
    dropout = tf.keras.layers.Dropout(0.3)(embedding)
    dense_1 = tf.keras.layers.Dense(512, activation="relu", kernel_initializer="he_normal")(dropout)
    batch_norm_1 = tf.keras.layers.BatchNormalization()(dense_1)
    dense_2 = tf.keras.layers.Dense(256, activation="relu", kernel_initializer="he_normal")(batch_norm_1)
    batch_norm_2 = tf.keras.layers.BatchNormalization()(dense_2)
    sister_output = tf.keras.layers.Dense(256)(batch_norm_2)
    sister_model = tf.keras.Model(image, sister_output)

    # Build siamese architecture
    image_1 = tf.keras.layers.Input((224, 224) + (3,))
    sister_1 = sister_model(image_1)

    image_2 = tf.keras.layers.Input((224, 224) + (3,))
    sister_2 = sister_model(image_2)

    merge = tf.keras.layers.Lambda(euclidean_distance)([sister_1, sister_2])
    batch_norm_3 = tf.keras.layers.BatchNormalization()(merge)
    output = tf.keras.layers.Dense(1, activation="sigmoid")(batch_norm_3)
    model = tf.keras.Model(inputs=[image_1, image_2], outputs=output)

    return model


def build_intermediate_fusion_model(fine_tuned_model, siamese_model):
    """
    Build model with Intermediate Fusion architecture
    :param fine_tuned_model: pre-trained Text Branch
    :param siamese_model: pre-trained Image Branch
    :return: uncompiled model
    """

    fine_tuned_model.trainable = False

    avg_pool = fine_tuned_model.get_layer("concatenate").input[0]
    max_pool = fine_tuned_model.get_layer("concatenate").input[1]

    siamese_model.trainable = False

    image_embedding_1 = siamese_model.get_layer("lambda").input[0]
    image_embedding_2 = siamese_model.get_layer("lambda").input[1]

    # Build architecture
    concat = tf.keras.layers.concatenate([image_embedding_1, image_embedding_2, avg_pool, max_pool])
    batch_norm = tf.keras.layers.BatchNormalization()(concat)
    dropout_1 = tf.keras.layers.Dropout(0.3)(batch_norm)
    dense_1 = tf.keras.layers.Dense(512, activation="relu", kernel_initializer="he_normal")(dropout_1)
    dropout_2 = tf.keras.layers.Dropout(0.3)(dense_1)
    dense_2 = tf.keras.layers.Dense(512, activation="relu", kernel_initializer="he_normal")(dropout_2)
    dropout_3 = tf.keras.layers.Dropout(0.3)(dense_2)
    dense_3 = tf.keras.layers.Dense(256, activation="relu", kernel_initializer="he_normal")(dropout_3)
    dropout_4 = tf.keras.layers.Dropout(0.3)(dense_3)
    output = tf.keras.layers.Dense(1, activation="sigmoid")(dropout_4)

    # Rename layers to have unique names
    for layer in siamese_model.layers[:2]:
        layer._name = layer.name + str("_2")

    model = tf.keras.models.Model(inputs=fine_tuned_model.input + siamese_model.input, outputs=output)

    return model


def build_valenciano_et_al_model():
    """
    Build model analog to Valenciano et al.
    :return: uncompiled model
    """

    # Load pre-trained ResNet
    embedding_network = tf.keras.applications.resnet.ResNet50(
        weights="imagenet",
        input_shape=(224, 224) + (3,),
        include_top=False
    )
    embedding_network.trainable = False

    pool = tf.keras.layers.GlobalAveragePooling2D()(embedding_network.output)
    embedding_network = tf.keras.Model(embedding_network.input, pool)

    image_1 = tf.keras.layers.Input((224, 224) + (3,))
    image_embedding_1 = embedding_network(image_1, training=False)

    image_2 = tf.keras.layers.Input((224, 224) + (3,))
    image_embedding_2 = embedding_network(image_2, training=False)

    # Load pre-trained BERT
    embedding_network = transformers.TFBertModel.from_pretrained("bert-base-cased")
    embedding_network.trainable = False

    input_ids = tf.keras.layers.Input(shape=(128,), dtype=tf.int32)
    attention_masks = tf.keras.layers.Input(shape=(128,), dtype=tf.int32)
    token_type_ids = tf.keras.layers.Input(shape=(128,), dtype=tf.int32)

    embedding_network_output = embedding_network.bert(
        input_ids,
        attention_mask=attention_masks,
        token_type_ids=token_type_ids
    )

    # Build architecture
    sequence_output = embedding_network_output.last_hidden_state
    bi_lstm = tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(64, return_sequences=True))(sequence_output)
    avg_pool = tf.keras.layers.GlobalAveragePooling1D()(bi_lstm)
    max_pool = tf.keras.layers.GlobalMaxPooling1D()(bi_lstm)
    concat = tf.keras.layers.concatenate([image_embedding_1, image_embedding_2, avg_pool, max_pool])

    batch_norm = tf.keras.layers.BatchNormalization()(concat)
    dropout_1 = tf.keras.layers.Dropout(0.3)(batch_norm)
    dense_1 = tf.keras.layers.Dense(512, activation="relu", kernel_initializer="he_normal")(dropout_1)
    dropout_2 = tf.keras.layers.Dropout(0.3)(dense_1)
    dense_2 = tf.keras.layers.Dense(512, activation="relu", kernel_initializer="he_normal")(dropout_2)
    dropout_3 = tf.keras.layers.Dropout(0.3)(dense_2)
    dense_3 = tf.keras.layers.Dense(128, activation="relu", kernel_initializer="he_normal")(dropout_3)
    dropout_4 = tf.keras.layers.Dropout(0.3)(dense_3)
    output = tf.keras.layers.Dense(1, activation='sigmoid')(dropout_4)

    model = tf.keras.Model(inputs=[input_ids, attention_masks, token_type_ids, image_1, image_2], outputs=output)

    return model

# Train compiled model
def fit(model, data_id, train_batches, val_batches, epochs):
    model.fit(
        train_batches,
        validation_data=val_batches,
        epochs=epochs,
        use_multiprocessing=True,
        workers=-1,
        class_weight=data_preparation.get_class_weights(data_id),
        callbacks=[callback]
    )

    return model


def train_fine_tuned_model(model, data_id, train_batches, val_batches, epochs, unfreeze=False):
    """
    Train Fine-tuned model
    :param model: model to train
    :param data_id: dataset to use for class weight calculation
    :param train_batches: batches for training
    :param val_batches: batches for validation
    :param epochs: number of epochs
    :param unfreeze: specifies whether to train embedding network
    :return: trained model
    """

    # Set training configuration
    if not unfreeze:
        learning_rate = 1e-3
    else:
        model.trainable = True
        learning_rate = 1e-5

    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate), loss="BinaryCrossentropy", metrics=metrics)
    model = fit(model, data_id, train_batches, val_batches, epochs)

    return model


# Calculate contrastive loss
def loss(margin=1):
    def contrastive_loss(y_true, y_pred):
        square_pred = tf.math.square(y_pred)
        margin_square = tf.math.square(tf.math.maximum(margin - (y_pred), 0))
        return tf.math.reduce_mean((1 - y_true) * square_pred + (y_true) * margin_square)

    return contrastive_loss


def train_siamese_model(model, data_id, train_batches, val_batches, epochs):
    """
    Train Siamese model
    :param model: model to train
    :param data_id: dataset to use for class weight calculation
    :param train_batches: batches for training
    :param val_batches: batches for validation
    :param epochs: number of epochs
    :return: trained model
    """
    model.compile(loss=loss(margin=1), optimizer=tf.keras.optimizers.RMSprop(), metrics=metrics)
    model = fit(model, data_id, train_batches, val_batches, epochs)

    return model


def train_intermediate_fusion_model(model, data_id, train_batches, val_batches, epochs):
    """
    Train Intermediate Fusion model (or Valenciano et al.'s model)
    :param model: model to train
    :param data_id: dataset to use for class weight calculation
    :param train_batches: batches for training
    :param val_batches: batches for validation
    :param epochs: number of epochs
    :return: trained model
    """
    model.compile(optimizer=tf.keras.optimizers.Adam(), loss="BinaryCrossentropy", metrics=metrics)
    model = fit(model, data_id, train_batches, val_batches, epochs)

    return model
